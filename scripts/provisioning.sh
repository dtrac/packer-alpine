#!/bin/sh -x

apk update add bash curl

rm -rf /var/cache/apk/*
rm -rf /etc/ssh/ssh_host_*

cat <<EOF >> /etc/ssh/sshd_config
UseDNS no
EOF